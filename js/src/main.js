var bonzo = require('bonzo');
var Carousel = require('lib/ui/carousel');
var domready = require('domready');
var Tabs = require('lib/ui/tabs');
var qwery = require('qwery');
var Secondarynav = require('lib/ui/secondarynav');

domready(function () {
    bonzo(qwery('.carousel')).each(function(element) {
        new Carousel(element);
    });

    bonzo(qwery('.tabs')).each(function(element) {
        new Tabs(element);
    });
});