(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
; var __browserify_shim_require__=require;(function browserifyShim(module, exports, require, define, browserify_shim__define__module__export__) {
/*! modernizr 3.3.1 (Custom Build) | MIT *
 * http://modernizr.com/download/?-csstransforms-csstransforms3d-csstransitions-touchevents-prefixed-prefixedcss-setclasses !*/
!function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,s,i,a;for(var f in C)if(C.hasOwnProperty(f)){if(e=[],n=C[f],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,s=0;s<e.length;s++)i=e[s],a=i.split("."),1===a.length?Modernizr[a[0]]=o:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=o),y.push((o?"":"no-")+a.join("-"))}}function s(e){var n=S.className,t=Modernizr._config.classPrefix||"";if(w&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),w?S.className.baseVal=n:S.className=n)}function i(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function a(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function f(e,n){return!!~(""+e).indexOf(n)}function u(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):w?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function l(e,n){return function(){return e.apply(n,arguments)}}function p(e,n,t){var o;for(var s in e)if(e[s]in n)return t===!1?e[s]:(o=n[e[s]],r(o,"function")?l(o,t||n):o);return!1}function d(){var e=n.body;return e||(e=u(w?"svg":"body"),e.fake=!0),e}function c(e,t,r,o){var s,i,a,f,l="modernizr",p=u("div"),c=d();if(parseInt(r,10))for(;r--;)a=u("div"),a.id=o?o[r]:l+(r+1),p.appendChild(a);return s=u("style"),s.type="text/css",s.id="s"+l,(c.fake?c:p).appendChild(s),c.appendChild(p),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(n.createTextNode(e)),p.id=l,c.fake&&(c.style.background="",c.style.overflow="hidden",f=S.style.overflow,S.style.overflow="hidden",S.appendChild(c)),i=t(p,e),c.fake?(c.parentNode.removeChild(c),S.style.overflow=f,S.offsetHeight):p.parentNode.removeChild(p),!!i}function m(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(a(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var s=[];o--;)s.push("("+a(n[o])+":"+r+")");return s=s.join(" or "),c("@supports ("+s+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return t}function v(e,n,o,s){function a(){p&&(delete N.style,delete N.modElem)}if(s=r(s,"undefined")?!1:s,!r(o,"undefined")){var l=m(e,o);if(!r(l,"undefined"))return l}for(var p,d,c,v,h,g=["modernizr","tspan"];!N.style;)p=!0,N.modElem=u(g.shift()),N.style=N.modElem.style;for(c=e.length,d=0;c>d;d++)if(v=e[d],h=N.style[v],f(v,"-")&&(v=i(v)),N.style[v]!==t){if(s||r(o,"undefined"))return a(),"pfx"==n?v:!0;try{N.style[v]=o}catch(y){}if(N.style[v]!=h)return a(),"pfx"==n?v:!0}return a(),!1}function h(e,n,t,o,s){var i=e.charAt(0).toUpperCase()+e.slice(1),a=(e+" "+z.join(i+" ")+i).split(" ");return r(n,"string")||r(n,"undefined")?v(a,n,o,s):(a=(e+" "+E.join(i+" ")+i).split(" "),p(a,n,t))}function g(e,n,r){return h(e,t,t,n,r)}var y=[],C=[],x={_version:"3.3.1",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){C.push({name:e,fn:n,options:t})},addAsyncTest:function(e){C.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=x,Modernizr=new Modernizr;var S=n.documentElement,w="svg"===S.nodeName.toLowerCase(),_="CSS"in e&&"supports"in e.CSS,b="supportsCSS"in e;Modernizr.addTest("supports",_||b);var T="Moz O ms Webkit",z=x._config.usePrefixes?T.split(" "):[];x._cssomPrefixes=z;var P=function(n){var r,o=L.length,s=e.CSSRule;if("undefined"==typeof s)return t;if(!n)return!1;if(n=n.replace(/^@/,""),r=n.replace(/-/g,"_").toUpperCase()+"_RULE",r in s)return"@"+n;for(var i=0;o>i;i++){var a=L[i],f=a.toUpperCase()+"_"+r;if(f in s)return"@-"+a.toLowerCase()+"-"+n}return!1};x.atRule=P;var E=x._config.usePrefixes?T.toLowerCase().split(" "):[];x._domPrefixes=E;var j=x.testStyles=c,k={elem:u("modernizr")};Modernizr._q.push(function(){delete k.elem});var N={style:k.elem.style};Modernizr._q.unshift(function(){delete N.style}),x.testAllProps=h;var A=x.prefixed=function(e,n,t){return 0===e.indexOf("@")?P(e):(-1!=e.indexOf("-")&&(e=i(e)),n?h(e,n,t):h(e,"pfx"))};x.prefixedCSS=function(e){var n=A(e);return n&&a(n)};x.testAllProps=g,Modernizr.addTest("csstransforms",function(){return-1===navigator.userAgent.indexOf("Android 2.")&&g("transform","scale(1)",!0)}),Modernizr.addTest("csstransforms3d",function(){var e=!!g("perspective","1px",!0),n=Modernizr._config.usePrefixes;if(e&&(!n||"webkitPerspective"in S.style)){var t,r="#modernizr{width:0;height:0}";Modernizr.supports?t="@supports (perspective: 1px)":(t="@media (transform-3d)",n&&(t+=",(-webkit-transform-3d)")),t+="{#modernizr{width:7px;height:18px;margin:0;padding:0;border:0}}",j(r+t,function(n){e=7===n.offsetWidth&&18===n.offsetHeight})}return e}),Modernizr.addTest("csstransitions",g("transition","all",!0));var L=x._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];x._prefixes=L,Modernizr.addTest("touchevents",function(){var t;if("ontouchstart"in e||e.DocumentTouch&&n instanceof DocumentTouch)t=!0;else{var r=["@media (",L.join("touch-enabled),("),"heartz",")","{#modernizr{top:9px;position:absolute}}"].join("");j(r,function(e){t=9===e.offsetTop})}return t}),o(),s(y),delete x.addTest,delete x.addAsyncTest;for(var O=0;O<Modernizr._q.length;O++)Modernizr._q[O]();e.Modernizr=Modernizr}(window,document);
; browserify_shim__define__module__export__(typeof Modernizr != "undefined" ? Modernizr : window.Modernizr);

}).call(global, undefined, undefined, undefined, undefined, function defineExport(ex) { module.exports = ex; });

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],2:[function(require,module,exports){
var bean = require('bean');
var bonzo = require('bonzo');
var domready = require('domready');
var Hammer = require('hammerjs');
var Modernizr = require('modernizr');
var qwery = require('qwery');

var options = {
    carouselSelector: '.carousel',
    paneSelector: 'li',
    sliderSelector: '.carousel__wrapper',
    transitionDuration: '300',
    transitionTimingFunction: 'ease-in-out',
    transitionTimingFunctionTouch: 'ease-out'
};

var DIRECTION_BACKWARD = 1,
    DIRECTION_FORWARD = 2;

function Carousel(container) {
    this.container = bonzo(qwery(container));
    this.slider = bonzo(qwery(options.sliderSelector, this.container));
    this.panes = bonzo(qwery(options.paneSelector, this.slider));

    // set up properties
    this.direction = DIRECTION_FORWARD;
    this.interaction = 'ontouchstart' in window ? 'touchstart' : 'click';
    this.events = {};
    this.indicators = [];
    this.currentPane = 0;

    // build and listen
    this._build();
    this._listen();
}

Carousel.prototype._addIndicator = function(item) {
    var indicator = bonzo(bonzo.create('<span>')).addClass('indicator');
    this.indicators.push(indicator);
    this.indicatorContainer.append(indicator);
}

Carousel.prototype._build = function() {
    this.touchHandler = new Hammer(this.slider[0], {
        drag_lock_to_axis: true
    });

    // count panes
    this.paneCount = this.panes.length;

    // get width of panes
    this.paneWidth = parseInt(this.container.css('width'));

    // capture ui controls
    this.btnNext = bonzo(qwery('.next', this.container));
    this.btnPrev = bonzo(qwery('.prev', this.container));

    this.indicatorContainer = bonzo(qwery('.indicator-container', this.container));

    if (this.paneCount > 1) {    
        // create indicators
        this.panes.each(this._addIndicator.bind(this));
        this._updateUI();
    } else {
        this.btnNext.css('display', 'none');
        this.btnPrev.css('display', 'none');
        this.indicatorContainer.css('display', 'none');
    }
}

Carousel.prototype._eventTouch = function(event) {
    event.preventDefault();

    switch(event.type) {
        case 'pan':
        case 'panleft':
        case 'panright':
            // stick to the finger
            var paneOffset = -this.paneWidth * this.currentPane;
            var dragOffset = event.deltaX;

            // slow down at the first and last pane
            if((this.currentPane == 0 && event.direction == Hammer.DIRECTION_RIGHT) ||
                    (this.currentPane == this.paneCount - 1 && event.direction == Hammer.DIRECTION_LEFT)) {
                dragOffset *= .4;
            }

            this._setOffset(dragOffset + paneOffset, 0, false);
            break;

        case 'panend':
            // more then 50% moved, navigate
            if (Math.abs(event.deltaX) > this.paneWidth * .4) {
                if (event.direction == 'right') {
                    this._showPane(this.currentPane - 1, this._getRemainingDuration(event));
                } else {
                    this._showPane(this.currentPane + 1, this._getRemainingDuration(event));
                }
            } else {
                // bounce back
                this._showPane(this.currentPane);
            }
            break;

        case 'swipeleft':
            this._showPane(this.currentPane + 1, this._getRemainingDuration(event));
            break;

        case 'swiperight':
            this._showPane(this.currentPane - 1, this._getRemainingDuration(event));
            break;
    }
}

Carousel.prototype._eventNext = function(event) {
    this._showPane(this.currentPane + 1);
    this.direction = DIRECTION_FORWARD;
}

Carousel.prototype._eventResize = function(event) {
    this.paneWidth = parseInt(this.container.css('width'));
    this._showPane(this.currentPane, 0, false);
}

Carousel.prototype._eventPrev = function(event) {
    this._showPane(this.currentPane - 1);
    this.direction = DIRECTION_BACKWARD;
}

Carousel.prototype._getRemainingDuration = function(gesture) {
    return Math.abs(Math.min(
        options.transitionDuration,
        (this.paneWidth - gesture.distance) / gesture.velocityX
    ));
}

Carousel.prototype._listen = function() {
    if (this.paneCount <= 1) {
        return;
    }

    this.events.touch = this._eventTouch.bind(this);
    this.touchHandler.on(
        'pan panend swipeleft swiperight',
        this.events.touch
    );

    this.events.resize = this._eventResize.bind(this);
    bean.on(window, 'resize', this.events.resize);

    if (this.btnNext) {
        this.events.next = this._eventNext.bind(this);
        bean.on(this.btnNext[0], this.interaction, this.events.next);
    }

    if (this.btnPrev) {
        this.events.prev = this._eventPrev.bind(this);
        bean.on(this.btnPrev[0], this.interaction, this.events.prev);
    }
}

Carousel.prototype._setOffset = function(offset, duration, animate) {
    this.slider.removeClass('animate');

    if (true === animate) {
        this.slider.addClass('animate');
    }

    var styles = {};

    if (Modernizr.csstransforms3d) {
        styles = {
            'transform': 'translate3d(' + offset + 'px,0,0) scale3d(1,1,1)'
        }
    } else if (Modernizr.csstransforms) {
       styles = {
           'transform': 'translate(' + offset + 'px,0)'
       }
    } else {
       styles = {'left': offset + 'px'}
    }

    styles[Modernizr.prefixed('transition')] = duration + 'ms'
    this.slider.css(styles);
}

Carousel.prototype._showPane = function(pane, duration, animate) {
    var index = Math.max(0, Math.min(pane, this.paneCount - 1)),
        duration = 'undefined' !== typeof duration ? duration : options.transitionDuration,
        animate = 'undefined' !== typeof animate ? animate : true;

    this.currentPane = index;

    // out of bounds - use fixed duration for bounce back
    if (index !== pane) {
        duration = options.transitionDuration;
    }

    var offset = -(this.paneWidth * this.currentPane);
    this._setOffset(offset, duration, animate);

    this._updateUI();
}

Carousel.prototype._updateUI = function() {
    this.indicators[this.currentPane].addClass('indicator-current');

    if (this.indicators[this.currentPane - 1]) {
        this.indicators[this.currentPane - 1].removeClass('indicator-current');
    }

    if (this.indicators[this.currentPane + 1]) {
        this.indicators[this.currentPane + 1].removeClass('indicator-current');
    }
}

module.exports = Carousel;
},{"bean":"bean","bonzo":"bonzo","domready":"domready","hammerjs":"hammerjs","modernizr":1,"qwery":"qwery"}],3:[function(require,module,exports){
// Where el is the DOM element being tested for visibility
function isHidden(el) {
    var style = window.getComputedStyle(el);
    return (style.display === 'none')
}

// Menu block
var
    menu = document.getElementById('drop-down'),
    el = document.getElementById('to-nav')
;

el.addEventListener('click', menuToggle);

function menuToggle(e) {
    if (isHidden(menu)) {
        //document.getElementById('drop-down').className =
        menu.className = 'secondary-nav';
    }
}

var x = document.getElementById('close-menu');
x.addEventListener('click', menuHide);

function menuHide (e) {
   menu.className = 'secondary-nav menu-hidden';
}


},{}],4:[function(require,module,exports){
var bean = require('bean');
var bonzo = require('bonzo');
var qwery = require('qwery');

var options = {
    panelSelector: '.tabContent',
    tabSelector: '.tab a'
};

function Tabs(container) {
    this.container = bonzo(qwery(container));
    this.panels = bonzo(qwery(options.panelSelector, this.container));

    this.events = {};

    // build and listen
    this._build();
    this._listen();

    this.showTab('#' + this.panels[0].id);
}

Tabs.prototype._build = function() {
    this.panels.hide();
    bonzo(this.panels[0]).show();
};

Tabs.prototype._listen = function() {
    this.events.onTabClick = this._onTabClick.bind(this);
    bean.on(this.container[0], 'click', options.tabSelector, this.events.onTabClick);
};

Tabs.prototype._onTabClick = function(event) {
    event.preventDefault();
    this.showTab(event.currentTarget.hash);
};

Tabs.prototype.showTab = function(selector) {
    this.currentTab = selector;
    this.updateUI();
};

Tabs.prototype.updateUI = function() {
    this.panels.hide();
    bonzo(qwery('.tabSelected')).removeClass('tabSelected');
    bonzo(qwery(this.currentTab)).show();
    bonzo(qwery('a[href="' + this.currentTab + '"]', this.container)).parent().addClass('tabSelected');
};

module.exports = Tabs;
},{"bean":"bean","bonzo":"bonzo","qwery":"qwery"}],5:[function(require,module,exports){
var bonzo = require('bonzo');
var Carousel = require('lib/ui/carousel');
var domready = require('domready');
var Tabs = require('lib/ui/tabs');
var qwery = require('qwery');
var Secondarynav = require('lib/ui/secondarynav');

domready(function () {
    bonzo(qwery('.carousel')).each(function(element) {
        new Carousel(element);
    });

    bonzo(qwery('.tabs')).each(function(element) {
        new Tabs(element);
    });
});
},{"bonzo":"bonzo","domready":"domready","lib/ui/carousel":2,"lib/ui/secondarynav":3,"lib/ui/tabs":4,"qwery":"qwery"}]},{},[5])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJsaWIvbW9kZXJuaXpyLmpzIiwic3JjL2xpYi91aS9jYXJvdXNlbC5qcyIsInNyYy9saWIvdWkvc2Vjb25kYXJ5bmF2LmpzIiwic3JjL2xpYi91aS90YWJzLmpzIiwic3JjL21haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ1BBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3hOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzVCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCI7IHZhciBfX2Jyb3dzZXJpZnlfc2hpbV9yZXF1aXJlX189cmVxdWlyZTsoZnVuY3Rpb24gYnJvd3NlcmlmeVNoaW0obW9kdWxlLCBleHBvcnRzLCByZXF1aXJlLCBkZWZpbmUsIGJyb3dzZXJpZnlfc2hpbV9fZGVmaW5lX19tb2R1bGVfX2V4cG9ydF9fKSB7XG4vKiEgbW9kZXJuaXpyIDMuMy4xIChDdXN0b20gQnVpbGQpIHwgTUlUICpcbiAqIGh0dHA6Ly9tb2Rlcm5penIuY29tL2Rvd25sb2FkLz8tY3NzdHJhbnNmb3Jtcy1jc3N0cmFuc2Zvcm1zM2QtY3NzdHJhbnNpdGlvbnMtdG91Y2hldmVudHMtcHJlZml4ZWQtcHJlZml4ZWRjc3Mtc2V0Y2xhc3NlcyAhKi9cbiFmdW5jdGlvbihlLG4sdCl7ZnVuY3Rpb24gcihlLG4pe3JldHVybiB0eXBlb2YgZT09PW59ZnVuY3Rpb24gbygpe3ZhciBlLG4sdCxvLHMsaSxhO2Zvcih2YXIgZiBpbiBDKWlmKEMuaGFzT3duUHJvcGVydHkoZikpe2lmKGU9W10sbj1DW2ZdLG4ubmFtZSYmKGUucHVzaChuLm5hbWUudG9Mb3dlckNhc2UoKSksbi5vcHRpb25zJiZuLm9wdGlvbnMuYWxpYXNlcyYmbi5vcHRpb25zLmFsaWFzZXMubGVuZ3RoKSlmb3IodD0wO3Q8bi5vcHRpb25zLmFsaWFzZXMubGVuZ3RoO3QrKyllLnB1c2gobi5vcHRpb25zLmFsaWFzZXNbdF0udG9Mb3dlckNhc2UoKSk7Zm9yKG89cihuLmZuLFwiZnVuY3Rpb25cIik/bi5mbigpOm4uZm4scz0wO3M8ZS5sZW5ndGg7cysrKWk9ZVtzXSxhPWkuc3BsaXQoXCIuXCIpLDE9PT1hLmxlbmd0aD9Nb2Rlcm5penJbYVswXV09bzooIU1vZGVybml6clthWzBdXXx8TW9kZXJuaXpyW2FbMF1daW5zdGFuY2VvZiBCb29sZWFufHwoTW9kZXJuaXpyW2FbMF1dPW5ldyBCb29sZWFuKE1vZGVybml6clthWzBdXSkpLE1vZGVybml6clthWzBdXVthWzFdXT1vKSx5LnB1c2goKG8/XCJcIjpcIm5vLVwiKSthLmpvaW4oXCItXCIpKX19ZnVuY3Rpb24gcyhlKXt2YXIgbj1TLmNsYXNzTmFtZSx0PU1vZGVybml6ci5fY29uZmlnLmNsYXNzUHJlZml4fHxcIlwiO2lmKHcmJihuPW4uYmFzZVZhbCksTW9kZXJuaXpyLl9jb25maWcuZW5hYmxlSlNDbGFzcyl7dmFyIHI9bmV3IFJlZ0V4cChcIihefFxcXFxzKVwiK3QrXCJuby1qcyhcXFxcc3wkKVwiKTtuPW4ucmVwbGFjZShyLFwiJDFcIit0K1wianMkMlwiKX1Nb2Rlcm5penIuX2NvbmZpZy5lbmFibGVDbGFzc2VzJiYobis9XCIgXCIrdCtlLmpvaW4oXCIgXCIrdCksdz9TLmNsYXNzTmFtZS5iYXNlVmFsPW46Uy5jbGFzc05hbWU9bil9ZnVuY3Rpb24gaShlKXtyZXR1cm4gZS5yZXBsYWNlKC8oW2Etel0pLShbYS16XSkvZyxmdW5jdGlvbihlLG4sdCl7cmV0dXJuIG4rdC50b1VwcGVyQ2FzZSgpfSkucmVwbGFjZSgvXi0vLFwiXCIpfWZ1bmN0aW9uIGEoZSl7cmV0dXJuIGUucmVwbGFjZSgvKFtBLVpdKS9nLGZ1bmN0aW9uKGUsbil7cmV0dXJuXCItXCIrbi50b0xvd2VyQ2FzZSgpfSkucmVwbGFjZSgvXm1zLS8sXCItbXMtXCIpfWZ1bmN0aW9uIGYoZSxuKXtyZXR1cm4hIX4oXCJcIitlKS5pbmRleE9mKG4pfWZ1bmN0aW9uIHUoKXtyZXR1cm5cImZ1bmN0aW9uXCIhPXR5cGVvZiBuLmNyZWF0ZUVsZW1lbnQ/bi5jcmVhdGVFbGVtZW50KGFyZ3VtZW50c1swXSk6dz9uLmNyZWF0ZUVsZW1lbnROUy5jYWxsKG4sXCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiLGFyZ3VtZW50c1swXSk6bi5jcmVhdGVFbGVtZW50LmFwcGx5KG4sYXJndW1lbnRzKX1mdW5jdGlvbiBsKGUsbil7cmV0dXJuIGZ1bmN0aW9uKCl7cmV0dXJuIGUuYXBwbHkobixhcmd1bWVudHMpfX1mdW5jdGlvbiBwKGUsbix0KXt2YXIgbztmb3IodmFyIHMgaW4gZSlpZihlW3NdaW4gbilyZXR1cm4gdD09PSExP2Vbc106KG89bltlW3NdXSxyKG8sXCJmdW5jdGlvblwiKT9sKG8sdHx8bik6byk7cmV0dXJuITF9ZnVuY3Rpb24gZCgpe3ZhciBlPW4uYm9keTtyZXR1cm4gZXx8KGU9dSh3P1wic3ZnXCI6XCJib2R5XCIpLGUuZmFrZT0hMCksZX1mdW5jdGlvbiBjKGUsdCxyLG8pe3ZhciBzLGksYSxmLGw9XCJtb2Rlcm5penJcIixwPXUoXCJkaXZcIiksYz1kKCk7aWYocGFyc2VJbnQociwxMCkpZm9yKDtyLS07KWE9dShcImRpdlwiKSxhLmlkPW8/b1tyXTpsKyhyKzEpLHAuYXBwZW5kQ2hpbGQoYSk7cmV0dXJuIHM9dShcInN0eWxlXCIpLHMudHlwZT1cInRleHQvY3NzXCIscy5pZD1cInNcIitsLChjLmZha2U/YzpwKS5hcHBlbmRDaGlsZChzKSxjLmFwcGVuZENoaWxkKHApLHMuc3R5bGVTaGVldD9zLnN0eWxlU2hlZXQuY3NzVGV4dD1lOnMuYXBwZW5kQ2hpbGQobi5jcmVhdGVUZXh0Tm9kZShlKSkscC5pZD1sLGMuZmFrZSYmKGMuc3R5bGUuYmFja2dyb3VuZD1cIlwiLGMuc3R5bGUub3ZlcmZsb3c9XCJoaWRkZW5cIixmPVMuc3R5bGUub3ZlcmZsb3csUy5zdHlsZS5vdmVyZmxvdz1cImhpZGRlblwiLFMuYXBwZW5kQ2hpbGQoYykpLGk9dChwLGUpLGMuZmFrZT8oYy5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGMpLFMuc3R5bGUub3ZlcmZsb3c9ZixTLm9mZnNldEhlaWdodCk6cC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHApLCEhaX1mdW5jdGlvbiBtKG4scil7dmFyIG89bi5sZW5ndGg7aWYoXCJDU1NcImluIGUmJlwic3VwcG9ydHNcImluIGUuQ1NTKXtmb3IoO28tLTspaWYoZS5DU1Muc3VwcG9ydHMoYShuW29dKSxyKSlyZXR1cm4hMDtyZXR1cm4hMX1pZihcIkNTU1N1cHBvcnRzUnVsZVwiaW4gZSl7Zm9yKHZhciBzPVtdO28tLTspcy5wdXNoKFwiKFwiK2EobltvXSkrXCI6XCIrcitcIilcIik7cmV0dXJuIHM9cy5qb2luKFwiIG9yIFwiKSxjKFwiQHN1cHBvcnRzIChcIitzK1wiKSB7ICNtb2Rlcm5penIgeyBwb3NpdGlvbjogYWJzb2x1dGU7IH0gfVwiLGZ1bmN0aW9uKGUpe3JldHVyblwiYWJzb2x1dGVcIj09Z2V0Q29tcHV0ZWRTdHlsZShlLG51bGwpLnBvc2l0aW9ufSl9cmV0dXJuIHR9ZnVuY3Rpb24gdihlLG4sbyxzKXtmdW5jdGlvbiBhKCl7cCYmKGRlbGV0ZSBOLnN0eWxlLGRlbGV0ZSBOLm1vZEVsZW0pfWlmKHM9cihzLFwidW5kZWZpbmVkXCIpPyExOnMsIXIobyxcInVuZGVmaW5lZFwiKSl7dmFyIGw9bShlLG8pO2lmKCFyKGwsXCJ1bmRlZmluZWRcIikpcmV0dXJuIGx9Zm9yKHZhciBwLGQsYyx2LGgsZz1bXCJtb2Rlcm5penJcIixcInRzcGFuXCJdOyFOLnN0eWxlOylwPSEwLE4ubW9kRWxlbT11KGcuc2hpZnQoKSksTi5zdHlsZT1OLm1vZEVsZW0uc3R5bGU7Zm9yKGM9ZS5sZW5ndGgsZD0wO2M+ZDtkKyspaWYodj1lW2RdLGg9Ti5zdHlsZVt2XSxmKHYsXCItXCIpJiYodj1pKHYpKSxOLnN0eWxlW3ZdIT09dCl7aWYoc3x8cihvLFwidW5kZWZpbmVkXCIpKXJldHVybiBhKCksXCJwZnhcIj09bj92OiEwO3RyeXtOLnN0eWxlW3ZdPW99Y2F0Y2goeSl7fWlmKE4uc3R5bGVbdl0hPWgpcmV0dXJuIGEoKSxcInBmeFwiPT1uP3Y6ITB9cmV0dXJuIGEoKSwhMX1mdW5jdGlvbiBoKGUsbix0LG8scyl7dmFyIGk9ZS5jaGFyQXQoMCkudG9VcHBlckNhc2UoKStlLnNsaWNlKDEpLGE9KGUrXCIgXCIrei5qb2luKGkrXCIgXCIpK2kpLnNwbGl0KFwiIFwiKTtyZXR1cm4gcihuLFwic3RyaW5nXCIpfHxyKG4sXCJ1bmRlZmluZWRcIik/dihhLG4sbyxzKTooYT0oZStcIiBcIitFLmpvaW4oaStcIiBcIikraSkuc3BsaXQoXCIgXCIpLHAoYSxuLHQpKX1mdW5jdGlvbiBnKGUsbixyKXtyZXR1cm4gaChlLHQsdCxuLHIpfXZhciB5PVtdLEM9W10seD17X3ZlcnNpb246XCIzLjMuMVwiLF9jb25maWc6e2NsYXNzUHJlZml4OlwiXCIsZW5hYmxlQ2xhc3NlczohMCxlbmFibGVKU0NsYXNzOiEwLHVzZVByZWZpeGVzOiEwfSxfcTpbXSxvbjpmdW5jdGlvbihlLG4pe3ZhciB0PXRoaXM7c2V0VGltZW91dChmdW5jdGlvbigpe24odFtlXSl9LDApfSxhZGRUZXN0OmZ1bmN0aW9uKGUsbix0KXtDLnB1c2goe25hbWU6ZSxmbjpuLG9wdGlvbnM6dH0pfSxhZGRBc3luY1Rlc3Q6ZnVuY3Rpb24oZSl7Qy5wdXNoKHtuYW1lOm51bGwsZm46ZX0pfX0sTW9kZXJuaXpyPWZ1bmN0aW9uKCl7fTtNb2Rlcm5penIucHJvdG90eXBlPXgsTW9kZXJuaXpyPW5ldyBNb2Rlcm5penI7dmFyIFM9bi5kb2N1bWVudEVsZW1lbnQsdz1cInN2Z1wiPT09Uy5ub2RlTmFtZS50b0xvd2VyQ2FzZSgpLF89XCJDU1NcImluIGUmJlwic3VwcG9ydHNcImluIGUuQ1NTLGI9XCJzdXBwb3J0c0NTU1wiaW4gZTtNb2Rlcm5penIuYWRkVGVzdChcInN1cHBvcnRzXCIsX3x8Yik7dmFyIFQ9XCJNb3ogTyBtcyBXZWJraXRcIix6PXguX2NvbmZpZy51c2VQcmVmaXhlcz9ULnNwbGl0KFwiIFwiKTpbXTt4Ll9jc3NvbVByZWZpeGVzPXo7dmFyIFA9ZnVuY3Rpb24obil7dmFyIHIsbz1MLmxlbmd0aCxzPWUuQ1NTUnVsZTtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgcylyZXR1cm4gdDtpZighbilyZXR1cm4hMTtpZihuPW4ucmVwbGFjZSgvXkAvLFwiXCIpLHI9bi5yZXBsYWNlKC8tL2csXCJfXCIpLnRvVXBwZXJDYXNlKCkrXCJfUlVMRVwiLHIgaW4gcylyZXR1cm5cIkBcIituO2Zvcih2YXIgaT0wO28+aTtpKyspe3ZhciBhPUxbaV0sZj1hLnRvVXBwZXJDYXNlKCkrXCJfXCIrcjtpZihmIGluIHMpcmV0dXJuXCJALVwiK2EudG9Mb3dlckNhc2UoKStcIi1cIitufXJldHVybiExfTt4LmF0UnVsZT1QO3ZhciBFPXguX2NvbmZpZy51c2VQcmVmaXhlcz9ULnRvTG93ZXJDYXNlKCkuc3BsaXQoXCIgXCIpOltdO3guX2RvbVByZWZpeGVzPUU7dmFyIGo9eC50ZXN0U3R5bGVzPWMsaz17ZWxlbTp1KFwibW9kZXJuaXpyXCIpfTtNb2Rlcm5penIuX3EucHVzaChmdW5jdGlvbigpe2RlbGV0ZSBrLmVsZW19KTt2YXIgTj17c3R5bGU6ay5lbGVtLnN0eWxlfTtNb2Rlcm5penIuX3EudW5zaGlmdChmdW5jdGlvbigpe2RlbGV0ZSBOLnN0eWxlfSkseC50ZXN0QWxsUHJvcHM9aDt2YXIgQT14LnByZWZpeGVkPWZ1bmN0aW9uKGUsbix0KXtyZXR1cm4gMD09PWUuaW5kZXhPZihcIkBcIik/UChlKTooLTEhPWUuaW5kZXhPZihcIi1cIikmJihlPWkoZSkpLG4/aChlLG4sdCk6aChlLFwicGZ4XCIpKX07eC5wcmVmaXhlZENTUz1mdW5jdGlvbihlKXt2YXIgbj1BKGUpO3JldHVybiBuJiZhKG4pfTt4LnRlc3RBbGxQcm9wcz1nLE1vZGVybml6ci5hZGRUZXN0KFwiY3NzdHJhbnNmb3Jtc1wiLGZ1bmN0aW9uKCl7cmV0dXJuLTE9PT1uYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoXCJBbmRyb2lkIDIuXCIpJiZnKFwidHJhbnNmb3JtXCIsXCJzY2FsZSgxKVwiLCEwKX0pLE1vZGVybml6ci5hZGRUZXN0KFwiY3NzdHJhbnNmb3JtczNkXCIsZnVuY3Rpb24oKXt2YXIgZT0hIWcoXCJwZXJzcGVjdGl2ZVwiLFwiMXB4XCIsITApLG49TW9kZXJuaXpyLl9jb25maWcudXNlUHJlZml4ZXM7aWYoZSYmKCFufHxcIndlYmtpdFBlcnNwZWN0aXZlXCJpbiBTLnN0eWxlKSl7dmFyIHQscj1cIiNtb2Rlcm5penJ7d2lkdGg6MDtoZWlnaHQ6MH1cIjtNb2Rlcm5penIuc3VwcG9ydHM/dD1cIkBzdXBwb3J0cyAocGVyc3BlY3RpdmU6IDFweClcIjoodD1cIkBtZWRpYSAodHJhbnNmb3JtLTNkKVwiLG4mJih0Kz1cIiwoLXdlYmtpdC10cmFuc2Zvcm0tM2QpXCIpKSx0Kz1cInsjbW9kZXJuaXpye3dpZHRoOjdweDtoZWlnaHQ6MThweDttYXJnaW46MDtwYWRkaW5nOjA7Ym9yZGVyOjB9fVwiLGoocit0LGZ1bmN0aW9uKG4pe2U9Nz09PW4ub2Zmc2V0V2lkdGgmJjE4PT09bi5vZmZzZXRIZWlnaHR9KX1yZXR1cm4gZX0pLE1vZGVybml6ci5hZGRUZXN0KFwiY3NzdHJhbnNpdGlvbnNcIixnKFwidHJhbnNpdGlvblwiLFwiYWxsXCIsITApKTt2YXIgTD14Ll9jb25maWcudXNlUHJlZml4ZXM/XCIgLXdlYmtpdC0gLW1vei0gLW8tIC1tcy0gXCIuc3BsaXQoXCIgXCIpOltcIlwiLFwiXCJdO3guX3ByZWZpeGVzPUwsTW9kZXJuaXpyLmFkZFRlc3QoXCJ0b3VjaGV2ZW50c1wiLGZ1bmN0aW9uKCl7dmFyIHQ7aWYoXCJvbnRvdWNoc3RhcnRcImluIGV8fGUuRG9jdW1lbnRUb3VjaCYmbiBpbnN0YW5jZW9mIERvY3VtZW50VG91Y2gpdD0hMDtlbHNle3ZhciByPVtcIkBtZWRpYSAoXCIsTC5qb2luKFwidG91Y2gtZW5hYmxlZCksKFwiKSxcImhlYXJ0elwiLFwiKVwiLFwieyNtb2Rlcm5penJ7dG9wOjlweDtwb3NpdGlvbjphYnNvbHV0ZX19XCJdLmpvaW4oXCJcIik7aihyLGZ1bmN0aW9uKGUpe3Q9OT09PWUub2Zmc2V0VG9wfSl9cmV0dXJuIHR9KSxvKCkscyh5KSxkZWxldGUgeC5hZGRUZXN0LGRlbGV0ZSB4LmFkZEFzeW5jVGVzdDtmb3IodmFyIE89MDtPPE1vZGVybml6ci5fcS5sZW5ndGg7TysrKU1vZGVybml6ci5fcVtPXSgpO2UuTW9kZXJuaXpyPU1vZGVybml6cn0od2luZG93LGRvY3VtZW50KTtcbjsgYnJvd3NlcmlmeV9zaGltX19kZWZpbmVfX21vZHVsZV9fZXhwb3J0X18odHlwZW9mIE1vZGVybml6ciAhPSBcInVuZGVmaW5lZFwiID8gTW9kZXJuaXpyIDogd2luZG93Lk1vZGVybml6cik7XG5cbn0pLmNhbGwoZ2xvYmFsLCB1bmRlZmluZWQsIHVuZGVmaW5lZCwgdW5kZWZpbmVkLCB1bmRlZmluZWQsIGZ1bmN0aW9uIGRlZmluZUV4cG9ydChleCkgeyBtb2R1bGUuZXhwb3J0cyA9IGV4OyB9KTtcbiIsInZhciBiZWFuID0gcmVxdWlyZSgnYmVhbicpO1xudmFyIGJvbnpvID0gcmVxdWlyZSgnYm9uem8nKTtcbnZhciBkb21yZWFkeSA9IHJlcXVpcmUoJ2RvbXJlYWR5Jyk7XG52YXIgSGFtbWVyID0gcmVxdWlyZSgnaGFtbWVyanMnKTtcbnZhciBNb2Rlcm5penIgPSByZXF1aXJlKCdtb2Rlcm5penInKTtcbnZhciBxd2VyeSA9IHJlcXVpcmUoJ3F3ZXJ5Jyk7XG5cbnZhciBvcHRpb25zID0ge1xuICAgIGNhcm91c2VsU2VsZWN0b3I6ICcuY2Fyb3VzZWwnLFxuICAgIHBhbmVTZWxlY3RvcjogJ2xpJyxcbiAgICBzbGlkZXJTZWxlY3RvcjogJy5jYXJvdXNlbF9fd3JhcHBlcicsXG4gICAgdHJhbnNpdGlvbkR1cmF0aW9uOiAnMzAwJyxcbiAgICB0cmFuc2l0aW9uVGltaW5nRnVuY3Rpb246ICdlYXNlLWluLW91dCcsXG4gICAgdHJhbnNpdGlvblRpbWluZ0Z1bmN0aW9uVG91Y2g6ICdlYXNlLW91dCdcbn07XG5cbnZhciBESVJFQ1RJT05fQkFDS1dBUkQgPSAxLFxuICAgIERJUkVDVElPTl9GT1JXQVJEID0gMjtcblxuZnVuY3Rpb24gQ2Fyb3VzZWwoY29udGFpbmVyKSB7XG4gICAgdGhpcy5jb250YWluZXIgPSBib256byhxd2VyeShjb250YWluZXIpKTtcbiAgICB0aGlzLnNsaWRlciA9IGJvbnpvKHF3ZXJ5KG9wdGlvbnMuc2xpZGVyU2VsZWN0b3IsIHRoaXMuY29udGFpbmVyKSk7XG4gICAgdGhpcy5wYW5lcyA9IGJvbnpvKHF3ZXJ5KG9wdGlvbnMucGFuZVNlbGVjdG9yLCB0aGlzLnNsaWRlcikpO1xuXG4gICAgLy8gc2V0IHVwIHByb3BlcnRpZXNcbiAgICB0aGlzLmRpcmVjdGlvbiA9IERJUkVDVElPTl9GT1JXQVJEO1xuICAgIHRoaXMuaW50ZXJhY3Rpb24gPSAnb250b3VjaHN0YXJ0JyBpbiB3aW5kb3cgPyAndG91Y2hzdGFydCcgOiAnY2xpY2snO1xuICAgIHRoaXMuZXZlbnRzID0ge307XG4gICAgdGhpcy5pbmRpY2F0b3JzID0gW107XG4gICAgdGhpcy5jdXJyZW50UGFuZSA9IDA7XG5cbiAgICAvLyBidWlsZCBhbmQgbGlzdGVuXG4gICAgdGhpcy5fYnVpbGQoKTtcbiAgICB0aGlzLl9saXN0ZW4oKTtcbn1cblxuQ2Fyb3VzZWwucHJvdG90eXBlLl9hZGRJbmRpY2F0b3IgPSBmdW5jdGlvbihpdGVtKSB7XG4gICAgdmFyIGluZGljYXRvciA9IGJvbnpvKGJvbnpvLmNyZWF0ZSgnPHNwYW4+JykpLmFkZENsYXNzKCdpbmRpY2F0b3InKTtcbiAgICB0aGlzLmluZGljYXRvcnMucHVzaChpbmRpY2F0b3IpO1xuICAgIHRoaXMuaW5kaWNhdG9yQ29udGFpbmVyLmFwcGVuZChpbmRpY2F0b3IpO1xufVxuXG5DYXJvdXNlbC5wcm90b3R5cGUuX2J1aWxkID0gZnVuY3Rpb24oKSB7XG4gICAgdGhpcy50b3VjaEhhbmRsZXIgPSBuZXcgSGFtbWVyKHRoaXMuc2xpZGVyWzBdLCB7XG4gICAgICAgIGRyYWdfbG9ja190b19heGlzOiB0cnVlXG4gICAgfSk7XG5cbiAgICAvLyBjb3VudCBwYW5lc1xuICAgIHRoaXMucGFuZUNvdW50ID0gdGhpcy5wYW5lcy5sZW5ndGg7XG5cbiAgICAvLyBnZXQgd2lkdGggb2YgcGFuZXNcbiAgICB0aGlzLnBhbmVXaWR0aCA9IHBhcnNlSW50KHRoaXMuY29udGFpbmVyLmNzcygnd2lkdGgnKSk7XG5cbiAgICAvLyBjYXB0dXJlIHVpIGNvbnRyb2xzXG4gICAgdGhpcy5idG5OZXh0ID0gYm9uem8ocXdlcnkoJy5uZXh0JywgdGhpcy5jb250YWluZXIpKTtcbiAgICB0aGlzLmJ0blByZXYgPSBib256byhxd2VyeSgnLnByZXYnLCB0aGlzLmNvbnRhaW5lcikpO1xuXG4gICAgdGhpcy5pbmRpY2F0b3JDb250YWluZXIgPSBib256byhxd2VyeSgnLmluZGljYXRvci1jb250YWluZXInLCB0aGlzLmNvbnRhaW5lcikpO1xuXG4gICAgaWYgKHRoaXMucGFuZUNvdW50ID4gMSkgeyAgICBcbiAgICAgICAgLy8gY3JlYXRlIGluZGljYXRvcnNcbiAgICAgICAgdGhpcy5wYW5lcy5lYWNoKHRoaXMuX2FkZEluZGljYXRvci5iaW5kKHRoaXMpKTtcbiAgICAgICAgdGhpcy5fdXBkYXRlVUkoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmJ0bk5leHQuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgICAgdGhpcy5idG5QcmV2LmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgICAgIHRoaXMuaW5kaWNhdG9yQ29udGFpbmVyLmNzcygnZGlzcGxheScsICdub25lJyk7XG4gICAgfVxufVxuXG5DYXJvdXNlbC5wcm90b3R5cGUuX2V2ZW50VG91Y2ggPSBmdW5jdGlvbihldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBzd2l0Y2goZXZlbnQudHlwZSkge1xuICAgICAgICBjYXNlICdwYW4nOlxuICAgICAgICBjYXNlICdwYW5sZWZ0JzpcbiAgICAgICAgY2FzZSAncGFucmlnaHQnOlxuICAgICAgICAgICAgLy8gc3RpY2sgdG8gdGhlIGZpbmdlclxuICAgICAgICAgICAgdmFyIHBhbmVPZmZzZXQgPSAtdGhpcy5wYW5lV2lkdGggKiB0aGlzLmN1cnJlbnRQYW5lO1xuICAgICAgICAgICAgdmFyIGRyYWdPZmZzZXQgPSBldmVudC5kZWx0YVg7XG5cbiAgICAgICAgICAgIC8vIHNsb3cgZG93biBhdCB0aGUgZmlyc3QgYW5kIGxhc3QgcGFuZVxuICAgICAgICAgICAgaWYoKHRoaXMuY3VycmVudFBhbmUgPT0gMCAmJiBldmVudC5kaXJlY3Rpb24gPT0gSGFtbWVyLkRJUkVDVElPTl9SSUdIVCkgfHxcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMuY3VycmVudFBhbmUgPT0gdGhpcy5wYW5lQ291bnQgLSAxICYmIGV2ZW50LmRpcmVjdGlvbiA9PSBIYW1tZXIuRElSRUNUSU9OX0xFRlQpKSB7XG4gICAgICAgICAgICAgICAgZHJhZ09mZnNldCAqPSAuNDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgdGhpcy5fc2V0T2Zmc2V0KGRyYWdPZmZzZXQgKyBwYW5lT2Zmc2V0LCAwLCBmYWxzZSk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdwYW5lbmQnOlxuICAgICAgICAgICAgLy8gbW9yZSB0aGVuIDUwJSBtb3ZlZCwgbmF2aWdhdGVcbiAgICAgICAgICAgIGlmIChNYXRoLmFicyhldmVudC5kZWx0YVgpID4gdGhpcy5wYW5lV2lkdGggKiAuNCkge1xuICAgICAgICAgICAgICAgIGlmIChldmVudC5kaXJlY3Rpb24gPT0gJ3JpZ2h0Jykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zaG93UGFuZSh0aGlzLmN1cnJlbnRQYW5lIC0gMSwgdGhpcy5fZ2V0UmVtYWluaW5nRHVyYXRpb24oZXZlbnQpKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zaG93UGFuZSh0aGlzLmN1cnJlbnRQYW5lICsgMSwgdGhpcy5fZ2V0UmVtYWluaW5nRHVyYXRpb24oZXZlbnQpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIGJvdW5jZSBiYWNrXG4gICAgICAgICAgICAgICAgdGhpcy5fc2hvd1BhbmUodGhpcy5jdXJyZW50UGFuZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdzd2lwZWxlZnQnOlxuICAgICAgICAgICAgdGhpcy5fc2hvd1BhbmUodGhpcy5jdXJyZW50UGFuZSArIDEsIHRoaXMuX2dldFJlbWFpbmluZ0R1cmF0aW9uKGV2ZW50KSk7XG4gICAgICAgICAgICBicmVhaztcblxuICAgICAgICBjYXNlICdzd2lwZXJpZ2h0JzpcbiAgICAgICAgICAgIHRoaXMuX3Nob3dQYW5lKHRoaXMuY3VycmVudFBhbmUgLSAxLCB0aGlzLl9nZXRSZW1haW5pbmdEdXJhdGlvbihldmVudCkpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgfVxufVxuXG5DYXJvdXNlbC5wcm90b3R5cGUuX2V2ZW50TmV4dCA9IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgdGhpcy5fc2hvd1BhbmUodGhpcy5jdXJyZW50UGFuZSArIDEpO1xuICAgIHRoaXMuZGlyZWN0aW9uID0gRElSRUNUSU9OX0ZPUldBUkQ7XG59XG5cbkNhcm91c2VsLnByb3RvdHlwZS5fZXZlbnRSZXNpemUgPSBmdW5jdGlvbihldmVudCkge1xuICAgIHRoaXMucGFuZVdpZHRoID0gcGFyc2VJbnQodGhpcy5jb250YWluZXIuY3NzKCd3aWR0aCcpKTtcbiAgICB0aGlzLl9zaG93UGFuZSh0aGlzLmN1cnJlbnRQYW5lLCAwLCBmYWxzZSk7XG59XG5cbkNhcm91c2VsLnByb3RvdHlwZS5fZXZlbnRQcmV2ID0gZnVuY3Rpb24oZXZlbnQpIHtcbiAgICB0aGlzLl9zaG93UGFuZSh0aGlzLmN1cnJlbnRQYW5lIC0gMSk7XG4gICAgdGhpcy5kaXJlY3Rpb24gPSBESVJFQ1RJT05fQkFDS1dBUkQ7XG59XG5cbkNhcm91c2VsLnByb3RvdHlwZS5fZ2V0UmVtYWluaW5nRHVyYXRpb24gPSBmdW5jdGlvbihnZXN0dXJlKSB7XG4gICAgcmV0dXJuIE1hdGguYWJzKE1hdGgubWluKFxuICAgICAgICBvcHRpb25zLnRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgKHRoaXMucGFuZVdpZHRoIC0gZ2VzdHVyZS5kaXN0YW5jZSkgLyBnZXN0dXJlLnZlbG9jaXR5WFxuICAgICkpO1xufVxuXG5DYXJvdXNlbC5wcm90b3R5cGUuX2xpc3RlbiA9IGZ1bmN0aW9uKCkge1xuICAgIGlmICh0aGlzLnBhbmVDb3VudCA8PSAxKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLmV2ZW50cy50b3VjaCA9IHRoaXMuX2V2ZW50VG91Y2guYmluZCh0aGlzKTtcbiAgICB0aGlzLnRvdWNoSGFuZGxlci5vbihcbiAgICAgICAgJ3BhbiBwYW5lbmQgc3dpcGVsZWZ0IHN3aXBlcmlnaHQnLFxuICAgICAgICB0aGlzLmV2ZW50cy50b3VjaFxuICAgICk7XG5cbiAgICB0aGlzLmV2ZW50cy5yZXNpemUgPSB0aGlzLl9ldmVudFJlc2l6ZS5iaW5kKHRoaXMpO1xuICAgIGJlYW4ub24od2luZG93LCAncmVzaXplJywgdGhpcy5ldmVudHMucmVzaXplKTtcblxuICAgIGlmICh0aGlzLmJ0bk5leHQpIHtcbiAgICAgICAgdGhpcy5ldmVudHMubmV4dCA9IHRoaXMuX2V2ZW50TmV4dC5iaW5kKHRoaXMpO1xuICAgICAgICBiZWFuLm9uKHRoaXMuYnRuTmV4dFswXSwgdGhpcy5pbnRlcmFjdGlvbiwgdGhpcy5ldmVudHMubmV4dCk7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuYnRuUHJldikge1xuICAgICAgICB0aGlzLmV2ZW50cy5wcmV2ID0gdGhpcy5fZXZlbnRQcmV2LmJpbmQodGhpcyk7XG4gICAgICAgIGJlYW4ub24odGhpcy5idG5QcmV2WzBdLCB0aGlzLmludGVyYWN0aW9uLCB0aGlzLmV2ZW50cy5wcmV2KTtcbiAgICB9XG59XG5cbkNhcm91c2VsLnByb3RvdHlwZS5fc2V0T2Zmc2V0ID0gZnVuY3Rpb24ob2Zmc2V0LCBkdXJhdGlvbiwgYW5pbWF0ZSkge1xuICAgIHRoaXMuc2xpZGVyLnJlbW92ZUNsYXNzKCdhbmltYXRlJyk7XG5cbiAgICBpZiAodHJ1ZSA9PT0gYW5pbWF0ZSkge1xuICAgICAgICB0aGlzLnNsaWRlci5hZGRDbGFzcygnYW5pbWF0ZScpO1xuICAgIH1cblxuICAgIHZhciBzdHlsZXMgPSB7fTtcblxuICAgIGlmIChNb2Rlcm5penIuY3NzdHJhbnNmb3JtczNkKSB7XG4gICAgICAgIHN0eWxlcyA9IHtcbiAgICAgICAgICAgICd0cmFuc2Zvcm0nOiAndHJhbnNsYXRlM2QoJyArIG9mZnNldCArICdweCwwLDApIHNjYWxlM2QoMSwxLDEpJ1xuICAgICAgICB9XG4gICAgfSBlbHNlIGlmIChNb2Rlcm5penIuY3NzdHJhbnNmb3Jtcykge1xuICAgICAgIHN0eWxlcyA9IHtcbiAgICAgICAgICAgJ3RyYW5zZm9ybSc6ICd0cmFuc2xhdGUoJyArIG9mZnNldCArICdweCwwKSdcbiAgICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgICBzdHlsZXMgPSB7J2xlZnQnOiBvZmZzZXQgKyAncHgnfVxuICAgIH1cblxuICAgIHN0eWxlc1tNb2Rlcm5penIucHJlZml4ZWQoJ3RyYW5zaXRpb24nKV0gPSBkdXJhdGlvbiArICdtcydcbiAgICB0aGlzLnNsaWRlci5jc3Moc3R5bGVzKTtcbn1cblxuQ2Fyb3VzZWwucHJvdG90eXBlLl9zaG93UGFuZSA9IGZ1bmN0aW9uKHBhbmUsIGR1cmF0aW9uLCBhbmltYXRlKSB7XG4gICAgdmFyIGluZGV4ID0gTWF0aC5tYXgoMCwgTWF0aC5taW4ocGFuZSwgdGhpcy5wYW5lQ291bnQgLSAxKSksXG4gICAgICAgIGR1cmF0aW9uID0gJ3VuZGVmaW5lZCcgIT09IHR5cGVvZiBkdXJhdGlvbiA/IGR1cmF0aW9uIDogb3B0aW9ucy50cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgIGFuaW1hdGUgPSAndW5kZWZpbmVkJyAhPT0gdHlwZW9mIGFuaW1hdGUgPyBhbmltYXRlIDogdHJ1ZTtcblxuICAgIHRoaXMuY3VycmVudFBhbmUgPSBpbmRleDtcblxuICAgIC8vIG91dCBvZiBib3VuZHMgLSB1c2UgZml4ZWQgZHVyYXRpb24gZm9yIGJvdW5jZSBiYWNrXG4gICAgaWYgKGluZGV4ICE9PSBwYW5lKSB7XG4gICAgICAgIGR1cmF0aW9uID0gb3B0aW9ucy50cmFuc2l0aW9uRHVyYXRpb247XG4gICAgfVxuXG4gICAgdmFyIG9mZnNldCA9IC0odGhpcy5wYW5lV2lkdGggKiB0aGlzLmN1cnJlbnRQYW5lKTtcbiAgICB0aGlzLl9zZXRPZmZzZXQob2Zmc2V0LCBkdXJhdGlvbiwgYW5pbWF0ZSk7XG5cbiAgICB0aGlzLl91cGRhdGVVSSgpO1xufVxuXG5DYXJvdXNlbC5wcm90b3R5cGUuX3VwZGF0ZVVJID0gZnVuY3Rpb24oKSB7XG4gICAgdGhpcy5pbmRpY2F0b3JzW3RoaXMuY3VycmVudFBhbmVdLmFkZENsYXNzKCdpbmRpY2F0b3ItY3VycmVudCcpO1xuXG4gICAgaWYgKHRoaXMuaW5kaWNhdG9yc1t0aGlzLmN1cnJlbnRQYW5lIC0gMV0pIHtcbiAgICAgICAgdGhpcy5pbmRpY2F0b3JzW3RoaXMuY3VycmVudFBhbmUgLSAxXS5yZW1vdmVDbGFzcygnaW5kaWNhdG9yLWN1cnJlbnQnKTtcbiAgICB9XG5cbiAgICBpZiAodGhpcy5pbmRpY2F0b3JzW3RoaXMuY3VycmVudFBhbmUgKyAxXSkge1xuICAgICAgICB0aGlzLmluZGljYXRvcnNbdGhpcy5jdXJyZW50UGFuZSArIDFdLnJlbW92ZUNsYXNzKCdpbmRpY2F0b3ItY3VycmVudCcpO1xuICAgIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBDYXJvdXNlbDsiLCIvLyBXaGVyZSBlbCBpcyB0aGUgRE9NIGVsZW1lbnQgYmVpbmcgdGVzdGVkIGZvciB2aXNpYmlsaXR5XG5mdW5jdGlvbiBpc0hpZGRlbihlbCkge1xuICAgIHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGVsKTtcbiAgICByZXR1cm4gKHN0eWxlLmRpc3BsYXkgPT09ICdub25lJylcbn1cblxuLy8gTWVudSBibG9ja1xudmFyXG4gICAgbWVudSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdkcm9wLWRvd24nKSxcbiAgICBlbCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0by1uYXYnKVxuO1xuXG5lbC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG1lbnVUb2dnbGUpO1xuXG5mdW5jdGlvbiBtZW51VG9nZ2xlKGUpIHtcbiAgICBpZiAoaXNIaWRkZW4obWVudSkpIHtcbiAgICAgICAgLy9kb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZHJvcC1kb3duJykuY2xhc3NOYW1lID1cbiAgICAgICAgbWVudS5jbGFzc05hbWUgPSAnc2Vjb25kYXJ5LW5hdic7XG4gICAgfVxufVxuXG52YXIgeCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjbG9zZS1tZW51Jyk7XG54LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgbWVudUhpZGUpO1xuXG5mdW5jdGlvbiBtZW51SGlkZSAoZSkge1xuICAgbWVudS5jbGFzc05hbWUgPSAnc2Vjb25kYXJ5LW5hdiBtZW51LWhpZGRlbic7XG59XG5cbiIsInZhciBiZWFuID0gcmVxdWlyZSgnYmVhbicpO1xudmFyIGJvbnpvID0gcmVxdWlyZSgnYm9uem8nKTtcbnZhciBxd2VyeSA9IHJlcXVpcmUoJ3F3ZXJ5Jyk7XG5cbnZhciBvcHRpb25zID0ge1xuICAgIHBhbmVsU2VsZWN0b3I6ICcudGFiQ29udGVudCcsXG4gICAgdGFiU2VsZWN0b3I6ICcudGFiIGEnXG59O1xuXG5mdW5jdGlvbiBUYWJzKGNvbnRhaW5lcikge1xuICAgIHRoaXMuY29udGFpbmVyID0gYm9uem8ocXdlcnkoY29udGFpbmVyKSk7XG4gICAgdGhpcy5wYW5lbHMgPSBib256byhxd2VyeShvcHRpb25zLnBhbmVsU2VsZWN0b3IsIHRoaXMuY29udGFpbmVyKSk7XG5cbiAgICB0aGlzLmV2ZW50cyA9IHt9O1xuXG4gICAgLy8gYnVpbGQgYW5kIGxpc3RlblxuICAgIHRoaXMuX2J1aWxkKCk7XG4gICAgdGhpcy5fbGlzdGVuKCk7XG5cbiAgICB0aGlzLnNob3dUYWIoJyMnICsgdGhpcy5wYW5lbHNbMF0uaWQpO1xufVxuXG5UYWJzLnByb3RvdHlwZS5fYnVpbGQgPSBmdW5jdGlvbigpIHtcbiAgICB0aGlzLnBhbmVscy5oaWRlKCk7XG4gICAgYm9uem8odGhpcy5wYW5lbHNbMF0pLnNob3coKTtcbn07XG5cblRhYnMucHJvdG90eXBlLl9saXN0ZW4gPSBmdW5jdGlvbigpIHtcbiAgICB0aGlzLmV2ZW50cy5vblRhYkNsaWNrID0gdGhpcy5fb25UYWJDbGljay5iaW5kKHRoaXMpO1xuICAgIGJlYW4ub24odGhpcy5jb250YWluZXJbMF0sICdjbGljaycsIG9wdGlvbnMudGFiU2VsZWN0b3IsIHRoaXMuZXZlbnRzLm9uVGFiQ2xpY2spO1xufTtcblxuVGFicy5wcm90b3R5cGUuX29uVGFiQ2xpY2sgPSBmdW5jdGlvbihldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgdGhpcy5zaG93VGFiKGV2ZW50LmN1cnJlbnRUYXJnZXQuaGFzaCk7XG59O1xuXG5UYWJzLnByb3RvdHlwZS5zaG93VGFiID0gZnVuY3Rpb24oc2VsZWN0b3IpIHtcbiAgICB0aGlzLmN1cnJlbnRUYWIgPSBzZWxlY3RvcjtcbiAgICB0aGlzLnVwZGF0ZVVJKCk7XG59O1xuXG5UYWJzLnByb3RvdHlwZS51cGRhdGVVSSA9IGZ1bmN0aW9uKCkge1xuICAgIHRoaXMucGFuZWxzLmhpZGUoKTtcbiAgICBib256byhxd2VyeSgnLnRhYlNlbGVjdGVkJykpLnJlbW92ZUNsYXNzKCd0YWJTZWxlY3RlZCcpO1xuICAgIGJvbnpvKHF3ZXJ5KHRoaXMuY3VycmVudFRhYikpLnNob3coKTtcbiAgICBib256byhxd2VyeSgnYVtocmVmPVwiJyArIHRoaXMuY3VycmVudFRhYiArICdcIl0nLCB0aGlzLmNvbnRhaW5lcikpLnBhcmVudCgpLmFkZENsYXNzKCd0YWJTZWxlY3RlZCcpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBUYWJzOyIsInZhciBib256byA9IHJlcXVpcmUoJ2JvbnpvJyk7XG52YXIgQ2Fyb3VzZWwgPSByZXF1aXJlKCdsaWIvdWkvY2Fyb3VzZWwnKTtcbnZhciBkb21yZWFkeSA9IHJlcXVpcmUoJ2RvbXJlYWR5Jyk7XG52YXIgVGFicyA9IHJlcXVpcmUoJ2xpYi91aS90YWJzJyk7XG52YXIgcXdlcnkgPSByZXF1aXJlKCdxd2VyeScpO1xudmFyIFNlY29uZGFyeW5hdiA9IHJlcXVpcmUoJ2xpYi91aS9zZWNvbmRhcnluYXYnKTtcblxuZG9tcmVhZHkoZnVuY3Rpb24gKCkge1xuICAgIGJvbnpvKHF3ZXJ5KCcuY2Fyb3VzZWwnKSkuZWFjaChmdW5jdGlvbihlbGVtZW50KSB7XG4gICAgICAgIG5ldyBDYXJvdXNlbChlbGVtZW50KTtcbiAgICB9KTtcblxuICAgIGJvbnpvKHF3ZXJ5KCcudGFicycpKS5lYWNoKGZ1bmN0aW9uKGVsZW1lbnQpIHtcbiAgICAgICAgbmV3IFRhYnMoZWxlbWVudCk7XG4gICAgfSk7XG59KTsiXX0=
